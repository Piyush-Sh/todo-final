import { createStore } from "redux";
import rootReducer from "./reducers";

//Store of redux
const store = createStore(
  rootReducer,
  //for using dev tool
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
