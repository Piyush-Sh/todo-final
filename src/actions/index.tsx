import { Fetching_User_Data } from "./action";
//define a action for getting user data
const fetchAction = (User: any) => {
  return {
    type: Fetching_User_Data,
    Userdata: User,
  };
};
export default fetchAction;
