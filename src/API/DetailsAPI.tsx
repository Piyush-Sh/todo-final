import { useEffect, useState } from "react";
import firebase from "../Firebase/firebase_config";
import { useDispatch } from "react-redux";
import fetchAction from "../actions/index";
//Custom hook for fetching the User detials data from firebase
const useDetailsAPI = () => {
  const [list, setList] = useState<any[]>([]);
  const dispatch = useDispatch();
  useEffect(() => {
    const Userdata = firebase.database().ref("User");
    Userdata.on("value", (snapshot) => {
      let USER = snapshot.val();
      const userList = [];
      if (window.localStorage.getItem("Info")) {
        for (const key in USER) {
          if (
            //Compare the Id of user for fetching the user data
            key === JSON.parse(Object(window.localStorage.getItem("Info"))).id
          ) {
            userList.push({
              id: key,
              email: String(USER[key].email),
              password: String(USER[key].password),
              Name: String(USER[key].Name),
              City: String(USER[key].City),
              State: String(USER[key].State),
              Country: String(USER[key].Country),
              Phone: String(USER[key].Phone),
              Imageurl: String(USER[key].Imageurl),
            });
          }
        }
        setList(userList);
        dispatch(fetchAction(userList));
      }
    });
  }, [dispatch]);

  return list;
};
export default useDetailsAPI;
