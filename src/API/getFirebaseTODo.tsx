import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import firebase from "../Firebase/firebase_config";

//Custom hooks for fetching the todo list data from firebase
const useGetToDo = () => {
  const [item, setitem] = useState<object[]>([]);
  const [isloading, setisloading] = useState<boolean>(true);
  const history = useHistory();

  useEffect(() => {
    setisloading(true);
    const todoref = firebase.database().ref("TODO");
    todoref.on("value", (snapshot) => {
      const todo = snapshot.val();
      const Items = [];
      for (let id in todo) {
        if (
          //Compare the User id and fetch the todo list data of the matched id
          todo[id].Infoid ===
          JSON.parse(Object(window.localStorage.getItem("Info"))).id
        ) {
          Items.push({ id, ...todo[id] });
        }
      }
      setitem(Items);
      setisloading(false);
    });
  }, [history]);
  return [JSON.parse(JSON.stringify(item)), isloading];
};
export default useGetToDo;
